# Navdy Nox Mounter

![icon](https://gitlab.com/navdy-hackers/Navdy-Nox-Mounter/raw/master/Navdy%20Nox%20Mounter/Assets.xcassets/icon_small.imageset/icon_small.png)

A macOS utility to help you mounting the Navdy device into the Navdy-MapDownloader (which runs inside Nox App Player)

  - macOS 10.10 or above
  - `bindfs` detection (if not installed, it will be installed through homebrew)
  - Nox App Player detection
  - Magic

![icon](https://gitlab.com/navdy-hackers/Navdy-Nox-Mounter/raw/master/preview.gif)

Download binary: [Navdy_Nox_Mounter_v1.0.zip](/uploads/0a73903e1c6aaf90d6c85b43e9ec28f7/Navdy_Nox_Mounter_v1.0.zip)
or clone this project and run from Xcode
