//
//  AppDelegate.swift
//  Navdy Nox Mounter
//
//  Created by Vai Kong Edward Li on 11/7/2018.
//  Copyright © 2018 Edward Li. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

