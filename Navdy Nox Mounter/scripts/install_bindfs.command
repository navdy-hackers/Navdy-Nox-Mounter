#!/bin/sh
which -s brew
if [[ $? != 0 ]] ; then
    # Install Homebrew
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    brew doctor
else
    brew update
fi

if [ ! -d "/Library/Filesystems/osxfuse.fs" ]; then
    echo "FUSE for macOS not installed, will install now"
    brew cask install osxfuse
fi

brew install bindfs
