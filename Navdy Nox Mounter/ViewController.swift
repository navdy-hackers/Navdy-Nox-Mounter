//
//  ViewController.swift
//  Navdy Nox Mounter
//
//  Created by Vai Kong Edward Li on 11/7/2018.
//  Copyright © 2018 Edward Li. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    @IBOutlet weak var dependencyTextField: NSTextField!
    @IBOutlet weak var actionButton: NSButton!
    
    var bindfsTimer: Timer?
    var isBindfsInstalled = false
    var bindfsPath: String?
    var isNoxReady = false
    var noxPath: String?
    var mountPath: String? {
        get {
            guard let path = noxPath else{
                return nil
            }
            return "\(path)/Navdy"
        }
    }
    var isNavdySelected = false
    var navdyPath: String?
    var isNavdyMounted = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        bindfsTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(checkBindfsInstalled), userInfo: nil, repeats: true)
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    func run(launchPath: String, arguments: [String]) -> (Int,String){
        let process = Process()
        process.launchPath = launchPath
        process.arguments = arguments
        let pipe = Pipe()
        process.standardOutput = pipe
        process.launch()
        process.waitUntilExit()
        let data = pipe.fileHandleForReading.readDataToEndOfFile()
        let outputString = String(data: data, encoding: String.Encoding.utf8) ?? ""
        return (Int(process.terminationStatus), outputString.trimmingCharacters(in: .whitespacesAndNewlines))
    }
    
    @objc func checkBindfsInstalled() {
        print("checking")

        let (status, output) = run(launchPath: "/bin/bash", arguments: ["-l", "-c", "which bindfs"])
        if status == 0, output.prefix(1) == "/" {
                bindfsPath = output
                bindfsTimer?.invalidate()
                isBindfsInstalled = true
            
                checkIfSharedFolderExists()
        } else {
            actionButton.isEnabled = true
            dependencyTextField.stringValue = "bindfs not installed"
            actionButton.title = "Install bindfs"
        }
    }
    
    func checkIfSharedFolderExists() {
        
        if let pathComponent = NSURL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("Library/Application Support/Nox App Player/Nox_share/Other") {
            if FileManager.default.fileExists(atPath: pathComponent.path) {
                noxPath = pathComponent.path
                
                let _ = unmountNavdy()
                isNoxReady = true
                dependencyTextField.stringValue = "Select your Navdy device"
                actionButton.title = "Select"
                actionButton.isEnabled = true
                return
            }
        }
        
        dependencyTextField.stringValue = "Please download and run Nox App Player first"
    }
    
    func mountNavdy() -> Int {
        guard createNavdyFolder() == 0 else {
            dependencyTextField.stringValue = "Error: cannot create mounting point"
            return 1
        }
        
        guard let bindfsPath = bindfsPath, let navdyPath = navdyPath, let mountPath = mountPath else {
            return 1
        }
        
        let (status, _) = run(launchPath: bindfsPath, arguments: [navdyPath, mountPath])
        
        if status == 0 {
            dependencyTextField.stringValue = "Navdy is mounted to Nox"
            actionButton.title = "Unmount"
            isNavdyMounted = true
        }else{
            dependencyTextField.stringValue = "Cannot mount Navdy device"
        }
        
        return status
    }
    
    func unmountNavdy() -> Int {
        guard let mountPath = mountPath else {
            return 1
        }
        let (status, _) = run(launchPath: "/sbin/umount", arguments: [mountPath])
        
        if status == 0 {
            return removeNavdyFolder()
        }
        return status
    }
    
    func createNavdyFolder() -> Int {
        guard let mountPath = mountPath else {
            return 1
        }
        
        let (status, _) = run(launchPath: "/bin/bash", arguments: ["-c", "mkdir -p '\(mountPath)'"])
        
        return status
    }
    
    func removeNavdyFolder() -> Int {
        guard let path = mountPath else {
            return 1
        }
        
        let (status, output) = run(launchPath: "/bin/bash", arguments: ["-c", "mount | grep Nox_share"])
        
        if status == 1 && output.isEmpty {
            //Safe to unmount
            let (rmStatus, _) = run(launchPath: "/bin/bash", arguments: ["-c", "rm -rf '\(path)'"])
            return rmStatus
        }else{
            return 1
        }
    }
    

    @IBAction func didClickActionButton(_ sender: NSButton) {
        if isBindfsInstalled == false {
            guard let path = Bundle.main.path(forResource: "install_bindfs", ofType: "command") else {
                return
            }
            NSWorkspace.shared.open(URL(fileURLWithPath: path))
        } else if isNavdyMounted {
            guard let navdyPath = navdyPath else {
                return
            }
            
            if unmountNavdy() == 0 {
                dependencyTextField.stringValue = "Navdy: \(navdyPath)"
                actionButton.title = "Mount Navdy to Nox"
                isNavdyMounted = false
            }else{
                dependencyTextField.stringValue = "Cannot unmount Navdy device\nPlease quit the MapDownloader first"
            }
        } else if isNavdySelected{
            
            //Check if Navdy is already mounted
            let (status, _) = run(launchPath: "/bin/bash", arguments: ["-c", "mount | grep Nox_share"])
            if status == 0 {
                dependencyTextField.stringValue = "Navdy is already mounted to Nox?"
                actionButton.title = "Unmount"
                isNavdyMounted = true
                return
            }
            
            let _ = mountNavdy()
            
        } else if isNoxReady {
            let openPanel = NSOpenPanel()
            openPanel.allowsMultipleSelection = false
            openPanel.canChooseDirectories = true
            openPanel.canCreateDirectories = false
            openPanel.canChooseFiles = false
            openPanel.begin { [weak self] (result) -> Void in
                
                if result == .OK {
                    if let url = openPanel.url{
                        self?.navdyPath = url.path
                        self?.isNavdySelected = true
                        self?.dependencyTextField.stringValue = "Navdy: \(url.path)"
                        self?.actionButton.title = "Mount Navdy to Nox"
                    }
                }
                
            }
        }
    }
}

